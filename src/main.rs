extern crate piston;
extern crate image;
extern crate num_traits;
extern crate num_cpus;
extern crate threadpool;

use std::env;
use piston_window::*;
use num_traits::cast::ToPrimitive;
use threadpool::ThreadPool;
use std::sync::mpsc::{channel, RecvError};

fn mand(x: f32, y: f32, iterations: u16, z: u16, x0: f32, y0: f32)
-> u16 {
    if z >= iterations {
        return iterations;
    } else if x0*x0 + y0*y0 <= 4.0 {
        return mand(x, y, iterations,
        z + 1, x0*x0 - y0*y0 + x, 2.0*y0*x0 + y);
    } else {
        return z;
    }
        
}

fn mand_px_scale(x: u16, y: u16, xmax: u16, ymax: u16) -> (f32, f32){
    (x.to_f32().expect("Conversion error!")*
    (3.5/xmax.to_f32().expect("Conversion error!")) - 2.5,
    y.to_f32().expect("Conversion error!")*
    (2.0/ymax.to_f32().expect("Conversion error!")) - 1.0)
}

fn color(num: u16, range: u16) -> (u8, u8, u8) {
    let val: u16 = (num.to_f32().expect("Conversion error!") *
                    (1024.0/range.to_f32().expect("Conversion error!")))
					.to_u16().expect("Conversion error!");

	match val {
		0 ... 255 => return (255,val.to_u8().unwrap_or(0),127),
		256 ... 511 => return (255-(val-256).to_u8().unwrap_or(0),255,127),
		512 ... 767 => return (127,255,(val-512).to_u8().unwrap_or(0)),
		_ => return (0,255 - (val-768).to_u8().unwrap_or(0),255),
	}
}

fn arg_parse(args: Vec<String>) -> (u32, u32){
	if args.len() > 1 {
		return
			(args[0].parse().unwrap_or(1280), args[1].parse().unwrap_or(720));
	}
	(1280, 720)
}

fn main() -> Result<(), RecvError> {

    let args: Vec<_> = env::args().skip(1).collect();
	let (width, height) = arg_parse(args);
    let iterations: u16 = 2048;

    let mut canvas = image::ImageBuffer::new(width, height);
    let pool = ThreadPool::new(num_cpus::get());
    let (tx, rx) = channel();

    for y in 0..height {
        let tx = tx.clone();
        pool.execute(move || for x in 0..width {
            let (x_scaled, y_scaled) = mand_px_scale(
                x.to_u16().expect("Conversion error!"),
                y.to_u16().expect("Conversion error!"),
                width.to_u16().expect("Conversion error!"),
                height.to_u16().expect("Conversion error!")
            );
            let z = mand(x_scaled, y_scaled, iterations - 1, 0, 0.0, 0.0);
            let (r,g,b) = color(z,iterations);
            let pixel = image::Rgba([r,g,b,255]);
            tx.send((x, y, pixel))
                    .expect("Could not send data!");
        });
    }

    for _ in 0..(width*height) {
        let (x,y,pixel) = rx.recv()?;
        canvas.put_pixel(x,y, pixel);
    }

    let mut window: PistonWindow =
        WindowSettings::new("mandelbrot", [width, height])
        .exit_on_esc(true).build().expect("Error building window!");

    let texture: G2dTexture = Texture::from_image(
        &mut window.factory,
        &canvas,
        &TextureSettings::new()
    ).expect("Conversion error!");

    while let Some(event) = window.next() {
        window.draw_2d(&event, |context, graphics| {
            clear([1.0;4], graphics);
            image(&texture,
                context.transform,
                graphics);
        });
    }

    Ok(())
}
